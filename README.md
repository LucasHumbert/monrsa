# MonRSA

## Auteurs
Lucas Humbert & Baptiste Houques

## Dépendances

Installation des dépendances nécessaires au projet

```
pip install -r requirements.txt
```

## Lancement

Pour optenir un détail des différentes options possibles, lancer cette commande à la racine du projet:

```
python monRSA.py help
```

### Génération des clés

Pour générer les clés privées et publiques, lancer:

```
python monRSA.py keygen
```

Différentes options peuvent suivre cette commande:

- **-f _nomDeFichier_** permet de choisir le nom des fichiers de clé générés, monRSA.pub et monRSA.priv par défaut.
- **-s _nombre_** permet de choisir la taille des clé générés, 10 par défaut.


### Encrypter un message

Pour encrypter un message, lancer:

```
python monRSA.py crypt [fichierCléPublique] [message]
```

Différentes options peuvent suivre cette commande:

- **-i _nomDuFichier_** permet de choisir un fichier d'entrée pour le cryptogramme.
- **-o _nomDeFichier_** permet de choisir un nom de fichier de sortie pour le cryptogramme.


### Decrypter un message

Pour decrypter un message, lancer:

```
python monRSA.py decrypt [fichierCléPrivée] [message]
```

Différentes options peuvent suivre cette commande:

- **-i _nomDuFichier_** permet de choisir un fichier d'entrée pour le cryptogramme.
- **-o _nomDeFichier_** permet de choisir un nom de fichier de sortie pour le cryptogramme.