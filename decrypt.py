import base64

def decrypt(privateKeyFile, stringToDecrypt, exportToFile=None):
    if not stringToDecrypt:
        print("Erreur: Message crypté vide")
        return

    f = open(privateKeyFile, "r")
    fileLines = f.read().split('\n')

    if fileLines[0] != "---begin monRSA private key---":
        print('invalid privatekey file')
        return
    
    base64Decoded = base64.b64decode(fileLines[1]).decode().split('\n')
    n = int(base64Decoded[0], 16)
    d = int(base64Decoded[1], 16)

    # decode base64 puis ASCII
    cryptedString = base64.b64decode(stringToDecrypt).decode('ascii')

    # découpage en blocs de taille n puis decryptage
    tailleBlocs = len(str(n))

    cryptedString = ' '.join(cryptedString[i:i+tailleBlocs] for i in range(0, len(cryptedString), tailleBlocs)) 
    blocs = cryptedString.split(' ')

    stringInAscii = ''
    for index, bloc in enumerate(blocs):
        bloc = str(pow(int(bloc), d, n))
        # rajouter les premiers 0 perdues lors de l'encodage des blocs sauf le premier, car 0 (potentiellement) fictifs
        if index != 0:
            rest = len(bloc) % (tailleBlocs - 1)
            if rest != 0:
                bloc = ('0' * ((tailleBlocs - 1) - rest)) + bloc

        stringInAscii += bloc

    # vérifier si découpable par blocs de 3 et rajouter des 0 au début
    # permet d'assurer qu'un code ASCII de moins de 3 de longueur au début décale les autres blocs
    rest = len(stringInAscii) % 3
    if rest != 0:
        stringInAscii = ('0' * (3 - rest)) + stringInAscii
    
    # découpage de la chaine ASCII en blocs de 3 de longueur afin d'obtenir les codes ASCII
    stringInAscii = ' '.join(stringInAscii[i:i+3] for i in range(0, len(stringInAscii), 3))
    blocs = stringInAscii.split(' ')

    # décodage des codes ASCII
    message = ''
    for bloc in blocs:
        message += chr(int(bloc))

    # Si l'utilisateur a renseigné un nom de fichier comme sortie.
    if exportToFile is not None:
        with open(f"{str(exportToFile)}.txt", "w") as txt_file:
            txt_file.write(message)
            print(str(exportToFile) + ".txt")

    # Comportement par défaut
    else:
        print(message)