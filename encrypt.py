import base64
import sys
sys.set_int_max_str_digits(0)

def encrypt(publicKeyFile, stringToEncrypt, exportToFile=None):
    if not stringToEncrypt:
        print("Erreur: Message vide")
        return

    f = open(publicKeyFile, "r")
    fileLines = f.read().split('\n')

    if fileLines[0] != "---begin monRSA public key---":
        print('invalid publickey file')
        return
    
    base64Decoded = base64.b64decode(fileLines[1]).decode().split('\n')
    n = int(base64Decoded[0], 16)
    e = int(base64Decoded[1], 16)

    # trim de la chaine en entrée
    stringToEncrypt = stringToEncrypt.strip()

    #transformer chaque caractere en ascii
    stringInAscii = ''
    for chars in stringToEncrypt:
        charInAscii = str(ord(chars))
        
        #si pas sur 3 charactères rajouter 0 devant
        if len(charInAscii) < 3:
            charInAscii = ('0' * (3 - len(charInAscii))) + charInAscii

        stringInAscii += charInAscii
    
    #découpage en blocs -> (taille de n) - 1
    tailleBlocs = len(str(n)) - 1

    rest = len(stringInAscii) % tailleBlocs

    # si reste pas égal à 0 donc pas découpable en blocs de tailles égales on rajoute des 0 au début
    if rest != 0:
        stringInAscii = ('0' * (tailleBlocs - rest)) + stringInAscii

    # separation en blocs, encryptage et assemblage dans une suite de chiffre
    stringInAscii = ' '.join(stringInAscii[i:i+tailleBlocs] for i in range(0, len(stringInAscii), tailleBlocs))

    blocs = stringInAscii.split(' ')
    suiteChiffre = ''

    for bloc in blocs:
        bloc = str(pow(int(bloc), e, n))

        rest = len(bloc) % len(str(n))
        if rest != 0:
            bloc = ('0' * (len(str(n)) - rest)) + bloc

        suiteChiffre += bloc

    # encodeage ASCII puis base64
    cryptedText = str(base64.b64encode(suiteChiffre.encode('ascii')))[2:-1]

    # Si l'utilisateur a renseigné un nom de fichier comme sortie.
    if exportToFile is not None:
        with open(f"{str(exportToFile)}.txt", "w") as txt_file:
            txt_file.write(cryptedText)
            print(str(exportToFile) + ".txt")

    # Comportement par défaut
    else:
        print(cryptedText)
