import sympy
import base64

def generate_prime_numbers(digits, exclude=[]):
    """Génère un nombre premier de la longueur de chiffres donnée, en excluant les nombres spécifiés."""
    while True:
        number = sympy.randprime(10**(digits-1), 10**digits)
        if number not in exclude:
            yield number

def find_e_and_d(totient):
    """Trouve les nombres e et d qui respectent les conditions."""
    for e in generate_prime_numbers(5):  # 5 chiffres juste comme exemple
        d = sympy.invert(e, totient)
        if d != e:
            return e, d
    return None, None

def generate_rsa_keys(keySize=10):
    # Générer 2 nombres premiers p & q de 10 chiffres
    prime_gen = generate_prime_numbers(keySize)
    p = next(prime_gen)

    # Générer q différent de p
    q = next(generate_prime_numbers(keySize, exclude=[p]))

    # Calculer n et n'
    n = p * q
    totient = (p - 1) * (q - 1)

    # Trouver e et d
    e, d = find_e_and_d(totient)

    # Retourner les clés
    return {
        "private_key": (n, d),
        "public_key": (n, e)
    }

def decimal_to_hex(num):
    return hex(num)[2:]

def save_keys_to_file(keys, prefix="monRSA"):
    private_key_content = "---begin monRSA private key---\n"
    private_key_content += base64.b64encode(
        (decimal_to_hex(keys['private_key'][0]) + '\n' + decimal_to_hex(keys['private_key'][1])).encode()).decode()
    private_key_content += "\n---end monRSA key---"

    with open(f"{prefix}.priv", "w") as priv_file:
        priv_file.write(private_key_content)

    public_key_content = "---begin monRSA public key---\n"
    public_key_content += base64.b64encode(
        (decimal_to_hex(keys['public_key'][0]) + '\n' + decimal_to_hex(keys['public_key'][1])).encode()).decode()
    public_key_content += "\n---end monRSA key---"

    with open(f"{prefix}.pub", "w") as pub_file:
        pub_file.write(public_key_content)

# if __name__ == "__main__":
#     keys = generate_rsa_keys()
#     print("Clé privée : ", keys["private_key"])
#     print("Clé publique : ", keys["public_key"])