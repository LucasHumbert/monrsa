import sys
import rsa_utils
from encrypt import encrypt
from decrypt import decrypt

def display_help():
    print("""Script monRSA par Nug
    Syntaxe :
    \tmonRSA <commande> [<clé>] [<texte>] [switchs].

    Commande :
    \tkeygen : Génère une paire de clé.
    \tcrypt : Chiffre <texte> pour le clé publique <clé>.
    \tdecrypt: Déchiffre <texte> pour le clé privée <clé>.
    \thelp : Affiche ce manuel.

    Clé :
    \tUn fichier qui contient une clé publique monRSA ("crypt") ou une clé privée ("decrypt").

    Texte :
    \tUne phrase en clair ("crypt") ou une phrase chiffrée ("decrypt").

    Switchs :
    \tkeygen:
        -f <file> permet de choisir le nom des fichiers de clé générés, monRSA.pub et monRSA.priv par défaut.
        -s <int> permet de choisir la taille des clé générés, 10 par défaut.
          
    \tcrypt:
        -i <string> permet de choisir un fichier d'entrée pour le cryptogramme.
        -o <string> permet de choisir un nom de fichier de sortie pour le cryptogramme.
          
    \tdecrypt:
        -i <string> permet de choisir un fichier d'entrée pour le cryptogramme.
        -o <string> permet de choisir un nom de fichier de sortie pour le cryptogramme.
    """)

def main():
    args = sys.argv

    if len(args) < 2 or args[1] == "help":
        display_help()
        return

    cmd = args[1]
    if cmd == "keygen":
        keys = 0
        filename = "monRSA"
        if "-f" in args:
            filename = args[args.index("-f")+1]
        if "-s" in args:
            size = int(args[args.index("-s")+1])
            if (size > 30):
                print("La taille des clés est de 30 maximum")
                return
            keys = rsa_utils.generate_rsa_keys(size)
        else:
            keys = rsa_utils.generate_rsa_keys()
        rsa_utils.save_keys_to_file(keys, filename)
    elif cmd in ["crypt", "decrypt"]:
        if len(args) < 4:
            print("Erreur: Paramètres insuffisants!")
            return
        
        if cmd == 'crypt':
            publicKey = args[2]
            
            if "-i" in args:
                fileToRead = args[args.index("-i")+1]
                try:
                    f = open(fileToRead, "r")
                except IOError:
                    print("Erreur: Le fichier n'existe pas.")
                    return
                
                text = f.read()
            else:
                text = args[3]

            exportToFile = None
            # Vérifie si un nom de fichier est demandé en sortie
            if "-o" in args:
                exportToFile = args[args.index("-o")+1]

            encrypt(publicKey, text, exportToFile)
        else:
            privateKey = args[2]
            
            if "-i" in args:
                fileToRead = args[args.index("-i")+1]
                try:
                    f = open(fileToRead, "r")
                except IOError:
                    print("Erreur: Le fichier n'existe pas.")
                    return
                
                text = f.read()
            else:
                text = args[3]

            exportToFile = None
            # Vérifie si un nom de fichier est demandé en sortie
            if "-o" in args:
                exportToFile = args[args.index("-o")+1]

            decrypt(privateKey, text, exportToFile)
    else:
        print("Erreur: Commande inconnue!")

if __name__ == "__main__":
    main()
